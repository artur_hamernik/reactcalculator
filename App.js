import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const App = () => {
  const onPress = () => functionCombined()

  return (
    <View style={styles.container}>
      <View style={styles.result}>
        <Text style={styles.resultText}>0</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>AC</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons0}>
          <Text style={styles.buttonText}></Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight}>
          <Text style={styles.buttonText}>/</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>7</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>8</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>9</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight}>
          <Text style={styles.buttonText}>X</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>4</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>5</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>6</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight}>
          <Text style={styles.buttonText}>-</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>1</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>2</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>3</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight}>
          <Text style={styles.buttonText}>+</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons0}>
          <Text style={styles.buttonText}>0</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons}>
          <Text style={styles.buttonText}>,</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight}>
          <Text style={styles.buttonText}>=</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display:"flex",
    flexWrap:"wrap",
    flex: 1,
    alignItems:"flex-start",
    justifyContent:"flex-start",
    backgroundColor:"#666666"
  },
  result:{
    display: "flex",
    height: "30%",
    width: "98.5%",
    marginHorizontal:3,
    backgroundColor:"#b36b00",
    justifyContent: "center",
    alignItems: "flex-end"
  },
  resultText:{
    display: "flex",
    alignSelf:"flex-end",
    fontSize: 100,
    color: "#ffffff"
  },
  buttonsContainer:{
    display: "flex",
    flex:1,
    display:"flex",
    flexWrap:"wrap",
    flexDirection:"row",
    padding:1,
  },
  buttons:{
    display:"flex",
    height: "19.6%",
    width: "24.5%",
    backgroundColor:"#000000",
    margin: 1,
    alignItems: "center",
    justifyContent:"center"
  },
  buttonText:{
    display: "flex",
    alignSelf:"center",
    fontSize: 28,
    color: "#ffffff"
  },
  buttons0:{
    display:"flex",
    height: "19.6%",
    width: "49.4%",
    backgroundColor:"#000000",
    margin: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    paddingHorizontal: 43
  },
  buttonsRight:{
    display:"flex",
    height: "19.6%",
    width: "24.5%",
    backgroundColor:"#000000",
    margin: 1,
    alignItems: "center",
    justifyContent:"center",
    backgroundColor:"#ff8c1a"
  }
});

export default App;
